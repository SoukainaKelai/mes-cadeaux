<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste des Articles</title>
</head>
<body>
	Bienvenu ${sessionScope.utilisateur.email} votre solde est de ${sessionScope.utilisateur.nbPoints}
	<h1>Liste des Articles</h1>
	<br>
	<c:if test="${sessionScope.utilisateur eq null}">
		<jsp:forward page="connexion.jsp"/>
	</c:if>
	<c:forEach var="article" items="${articles}">
		<h2>${article.designation} (${article.categorie.nom}) <a href="InfosServlet?id=${article.id}">+ d'infos</a>
		<c:choose>
			<c:when test="${article.stock == 0}">
				<div style="background-color: red; width: 20px; height: 20px;"></div>
			</c:when>
			<c:when test="${article.stock <5 && article.stock >0}">
				<div style="background-color: orange; width: 20px; height: 20px;"></div>
			</c:when>
			<c:otherwise>
				<div style="background-color: green; width: 20px; height: 20px;"></div>
			</c:otherwise>
		</c:choose>
		</h2>
	</c:forEach>
	<br>
	<br>
	<p>Nombre total d'article(s): ${articles.size()}</p><br>
	<a href="DeconnexionServlet">Se déconnecter</a>

</body>
</html>