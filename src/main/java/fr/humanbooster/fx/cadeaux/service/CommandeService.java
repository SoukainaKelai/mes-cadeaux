package fr.humanbooster.fx.cadeaux.service;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;

import java.util.List;

public interface CommandeService {

	public List<Commande> recupererCommandes();
	public Commande recupererCommandeParId(int id);
	public Commande ajouterCommande(int quantite, Article article, Utilisateur utilisateur);
}
