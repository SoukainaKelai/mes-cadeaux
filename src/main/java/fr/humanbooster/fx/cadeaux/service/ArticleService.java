package fr.humanbooster.fx.cadeaux.service;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;

public interface ArticleService {

	public List<Article> recupereArticles();

	public void ajouterArticle(String designation, int nbPoints, int stock);

  public Article recupererArticleParId(Long id);
}
