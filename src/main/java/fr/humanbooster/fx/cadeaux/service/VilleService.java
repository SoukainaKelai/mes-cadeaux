package fr.humanbooster.fx.cadeaux.service;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Ville;

public interface VilleService {

	public List<Ville> recupererVilles();
	public Ville recupererVilleParId(int idVille);
	public Ville ajouterVille(String nom);
}
