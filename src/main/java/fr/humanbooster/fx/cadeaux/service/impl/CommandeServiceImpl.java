package fr.humanbooster.fx.cadeaux.service.impl;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CommandeService;

import java.util.ArrayList;
import java.util.List;

public class CommandeServiceImpl implements CommandeService {
  private static List<Commande> commandes = new ArrayList<>();

  @Override
  public List<Commande> recupererCommandes() {
    return commandes;
  }

  @Override
  public Commande recupererCommandeParId(int id) {
    Commande commande = new Commande();
    if (commande.getId().equals(id)) {
      return commande;
    }
    return null;
  }

  @Override
  public Commande ajouterCommande(int quantite, Article article, Utilisateur utilisateur) {
    Commande commande = new Commande(quantite, article, utilisateur);
    commande.setArticle(article);
    commande.setQuantite(quantite);
    commande.setUtilisateur(utilisateur);
    commandes.add(commande);
    return commande;
  }
}
