package fr.humanbooster.fx.cadeaux.service;

import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;;

public interface UtilisateurService {

	public Utilisateur creerUtilisateur(String nom, String prenom, String email, String mdp, String string);
	public List<Utilisateur> recupererUtilisateurs();
	public Utilisateur recupererUtilisateurParId(Long id);

}
