package fr.humanbooster.fx.cadeaux.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;

public class UtilisateurServiceImpl implements UtilisateurService {

	private static List<Utilisateur> utilisateurs = new ArrayList<>();

	@Override
	public Utilisateur creerUtilisateur(String nom, String prenom, String email, String mdp, String idVille) {
		
		Ville villeSelectionnee = null;
		
		// la classe service est la seule classe autorisée à créer des objets métier
		Utilisateur utilisateur = new Utilisateur(nom, prenom, email, mdp, villeSelectionnee);
		utilisateurs.add(utilisateur);
		// TODO envoi d'un mail de confirmation pour valider son inscription
		return utilisateur;
	}

	@Override
	public List<Utilisateur> recupererUtilisateurs() {
		return utilisateurs;
	}

	@Override
	public Utilisateur recupererUtilisateurParId(Long id) {
		for (Utilisateur utilisateur : utilisateurs) {
			if (utilisateur.getId().equals(id)) {
				return utilisateur;
			}
		}
			return null;
	}
}
