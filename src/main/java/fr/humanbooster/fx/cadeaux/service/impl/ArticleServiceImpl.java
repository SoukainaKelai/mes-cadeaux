package fr.humanbooster.fx.cadeaux.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.ArticleService;

public class ArticleServiceImpl implements ArticleService {

	private static List<Article> articles = new ArrayList<>();

	@Override
	public List<Article> recupereArticles() {
		return articles;
	}

	@Override
	public void ajouterArticle(String designation, int nbPoints, int stock) {
		Article article = new Article();
		article.setDescription(designation);
		article.setDesignation(designation);
		article.setNbPoints(nbPoints);
		article.setStock(stock);
		articles.add(article);
	}

	@Override
	public Article recupererArticleParId(Long id) {
		for (Article article : articles) {
			if (article.getId().equals(id)) {
				return article;
			}
		}
			return null;
	}
}
