package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;
import fr.humanbooster.fx.cadeaux.service.impl.UtilisateurServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ConnexionServlet")
public class ConnexionServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  private UtilisateurService us = new UtilisateurServiceImpl();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//On parcourt la liste des clients pour trouver le bon client
    String emailSaisi = request.getParameter("EMAIL");
    String passSaisi = request.getParameter("MOT_DE_PASSE");
    for (Utilisateur utilisateur : us.recupererUtilisateurs()) {
      if (utilisateur.getEmail().equals(emailSaisi) && utilisateur.getMotDePasse().equals(passSaisi)) {
        //Le client a été trouvé
        //On crée une session HTTP pour ce client + redirection
        request.getSession().setAttribute("utilisateur", utilisateur);
        request.getRequestDispatcher("ArticlesServlet").forward(request, response);
        return;
      }
    }
  }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
          request.getRequestDispatcher("connexion.jsp").forward(request, response);
    }
}
