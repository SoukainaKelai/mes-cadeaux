package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(value = "/InfosServlet")
public class InfosServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  private ArticleService ar = new ArticleServiceImpl();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Long idArticleSelectionne = Long.parseLong(request.getParameter("id"));
    Article article = ar.recupererArticleParId(idArticleSelectionne);
    request.setAttribute("article", article);
    request.getRequestDispatcher("infos.jsp").forward(request, response);
  }
}
