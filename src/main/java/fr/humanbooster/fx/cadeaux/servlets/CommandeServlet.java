package fr.humanbooster.fx.cadeaux.servlets;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.business.Commande;
import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.CommandeService;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.CommandeServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.UtilisateurServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(value = "/CommandeServlet")
public class CommandeServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  private ArticleService ar = new ArticleServiceImpl();
  private CommandeService cs = new CommandeServiceImpl();
  private UtilisateurService us = new UtilisateurServiceImpl();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    int quantiteCommander = Integer.parseInt(request.getParameter("QUANTITE"));

    Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
    Long idUtilisateur = utilisateur.getId();

    Utilisateur utilisateurConnecter = us.recupererUtilisateurParId(idUtilisateur);
    Long idArticleSelectionne = Long.parseLong(request.getParameter("ID_ARTICLE"));
    Article articleChoisi = ar.recupererArticleParId(idArticleSelectionne);
    if (articleChoisi.getStock() >= quantiteCommander) {
      Commande commande = cs.ajouterCommande(quantiteCommander, articleChoisi, utilisateurConnecter);
      cs.ajouterCommande(quantiteCommander, articleChoisi, utilisateurConnecter);
      utilisateurConnecter.setNbPoints(utilisateurConnecter.getNbPoints() - articleChoisi.getNbPoints());
      System.out.println(utilisateurConnecter.getNbPoints());
      // on destocke l'article
      articleChoisi.setStock(articleChoisi.getStock() - quantiteCommander);
      request.setAttribute("commande", commande);
      request.getRequestDispatcher("MerciCommandeServlet").forward(request, response);
    }

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Long idArticleSelectionne = Long.parseLong(request.getParameter("id"));
    Article article = ar.recupererArticleParId(idArticleSelectionne);
    System.out.println("article" + article);
    request.setAttribute("article", article);
    request.getRequestDispatcher("commande.jsp").forward(request, response);
  }
}
