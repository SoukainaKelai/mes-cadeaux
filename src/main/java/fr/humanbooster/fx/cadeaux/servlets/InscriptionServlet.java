package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Utilisateur;
import fr.humanbooster.fx.cadeaux.business.Ville;
import fr.humanbooster.fx.cadeaux.service.UtilisateurService;
import fr.humanbooster.fx.cadeaux.service.VilleService;
import fr.humanbooster.fx.cadeaux.service.impl.UtilisateurServiceImpl;
import fr.humanbooster.fx.cadeaux.service.impl.VilleServiceImpl;

/**
 * Servlet implementation class InscriptionServlet
 */
@WebServlet(value = {"/InscriptionServlet", "/inscription"}, loadOnStartup = 1)
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService us = new UtilisateurServiceImpl();
	private VilleService vs = new VilleServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InscriptionServlet() {
		if (vs.recupererVilles().isEmpty()) {
		vs.ajouterVille("Casablanca");
		vs.ajouterVille("Fès");
		vs.ajouterVille("Tanger");
		vs.ajouterVille("Mohamedia");
		vs.ajouterVille("Lyon");
		}

		// Pour éviter de s'inscrire après chaque déploiement, on crée un utilisateur:
		if (us.recupererUtilisateurs().isEmpty())
		{
			us.creerUtilisateur("", "", "a@a.fr", "123", "1");
		}

		System.out.println(us.recupererUtilisateurs());

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// On envoit la liste des villes à la JSP inscription.jsp
		request.setAttribute("villes", vs.recupererVilles());
		request.getRequestDispatcher("inscription.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur utilisateur = us.creerUtilisateur(request.getParameter("NOM"), request.getParameter("PRENOM"),
				request.getParameter("EMAIL"), request.getParameter("MOT_DE_PASSE"), request.getParameter("ID_VILLE"));

		request.setAttribute("utilisateur", utilisateur);
		System.out.println(us.recupererUtilisateurs());
		request.getRequestDispatcher("merci.jsp").forward(request, response);
	}

}
