package fr.humanbooster.fx.cadeaux.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.humanbooster.fx.cadeaux.business.Article;
import fr.humanbooster.fx.cadeaux.service.ArticleService;
import fr.humanbooster.fx.cadeaux.service.impl.ArticleServiceImpl;

/**
 * Servlet implementation class ArticlesServlet
 */
@WebServlet(value= {"/ArticlesServlet","/catalogue", "/"}, loadOnStartup=1)
public class ArticlesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ArticleService as = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArticlesServlet() {
		super();
		as = new ArticleServiceImpl();
		if (as.recupereArticles().isEmpty())
		{
			as.ajouterArticle("Sandales Ted Lapidus", 20, 50);
			as.ajouterArticle("Moto BMW GS1200", 5000, 50);
			as.ajouterArticle("Montre Breitling", 20, 25);
			as.ajouterArticle("Ruche 10 cadres", 500, 10);
			as.ajouterArticle("Sac à main Chanel", 290, 10);
			as.ajouterArticle("Caméra GoPro", 100, 50);
			as.ajouterArticle("Skate", 100, 5);
			as.ajouterArticle("Guitar Fender Stratocaster", 1000, 2);
			as.ajouterArticle("Tesla", 20000, 2);
			as.ajouterArticle("Rubicube", 10, 20);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Recupere toutes les enquetes et les envoies a� la JSP
		List<Article> articles = as.recupereArticles();
		request.setAttribute("articles", articles);
		request.getRequestDispatcher("catalogue.jsp").include(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doDelete(req, resp);
	}
}
