<%--
  Created by IntelliJ IDEA.
  User: Belgacem
  Date: 2019-05-06
  Time: 11:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Page de connexion</title>
</head>
<body class="container">
<h1>Se connecter</h1>
<form action="ConnexionServlet" method="post">
    <input type="text" name="EMAIL" placeholder="exemple@exemple.exemple"><br>
    <input type="password" name="MOT_DE_PASSE" placeholder="Mot de passe"><br>
    <input type="submit" class="btn btn-dark">
    <a href="InscriptionServlet">S'inscrire</a>
</form>

</body>
</html>
