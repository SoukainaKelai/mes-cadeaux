<%--
  Created by IntelliJ IDEA.
  User: Belgacem
  Date: 2019-05-06
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Info Article</title>
</head>
<body>
<p>Nom : ${article.designation}</p>
<p>Stock : ${article.stock}</p>
<p>Prix : ${article.nbPoints}</p>
<button><a href="CommandeServlet?id=${article.id}">Commander</a></button>
<button><a href="ArticlesServlet">Retour</a></button>
</body>
</html>
